#!/bin/bash
#********************************************************************
# AUTOMATIC INSTALL AON INFRA REMOTE ENGINE AND RUNTIME
#********************************************************************
#    .---------- COLORS CONSTANT PART-------------------.
RED='\033[0;31m'
GRN='\033[0;32m'
YEL='\033[0;33m'
NC='\033[0m' # No Color

########################   Vars to enter before runs  #############################

APPS_DIRECTORY="/apps"
RE_PAIRING_REGIN="eu"
TLLD_USR_SERVICE='talend'
INSTALL_JAVA=$1
INSTALL_RE=$2
INSTALL_RT=$3
PAIRING_TOKEN=$4

#Talend-Runtime-V7.3.1
#Talend-RemoteEngine-V2.11.7

#on the real redhat check with sestatus if needed /etc/selinux/config SELINUX=disableed else yous should have the help of sysadmin


printf " ${GRN} ********************************************** STARTING INSTALLATION ************************************************************** ...  ${NC}\n"
## add the aon proxy int o the session
export https_proxy=http://serverproxy.rpp.aon.net:8080
printf " ${GRN} => 0. Creation of the Talend user Service $TLLD_USR_SERVICE  ... ${NC}\n"

useradd -m $TLLD_USR_SERVICE

echo "export https_proxy=http://serverproxy.rpp.aon.net:8080" >> /home/$TLLD_USR_SERVICE/.bash_profile

cd /

if [ -d "$APPS_DIRECTORY" ]; then
   #echo "'$APPS_DIRECTORY' found nothing to do ..."
   printf " ${GRN} => 1. The '$APPS_DIRECTORY' has been found nothing to do ... ${NC}\n"
else
   #echo "The  '$APPS_DIRECTORY' NOT found. it will be created "
   printf " ${GRN} => 1. The  '$APPS_DIRECTORY' NOT found. it will be created ${NC}\n"
   mkdir $APPS_DIRECTORY
fi

if [ "$INSTALL_JAVA" = true ] ; then
printf " ${GRN} => 2. Start downloading and installing Zulu jdk ..  ${NC}\n"
rm -rf $APPS_DIRECTORY/jvm/zulu8
mkdir -p $APPS_DIRECTORY/jvm/zulu8
cd  $APPS_DIRECTORY/jvm/zulu8
#wget --user-agent Mozilla/4.0 'https://gitlab.com/amar_belgacem/talend-cloud-binaries/-/raw/main/java.tar.gz' -O $APPS_DIRECTORY/jvm/zulu8/java.tar.gz
wget --user-agent Mozilla/4.0 'https://gitlab.com/amar_belgacem/talend-cloud-binaries/-/raw/main/java.tar.gz'
tar -xf java.tar.gz
mv zulu8.58.0.13-ca-jdk8.0.312-linux_x64 java
rm -f java.tar.gz
printf " ${GRN} => 3. Start Setting  java_home and path environnement varriable..  ${NC}\n"

# set java_home and path environnement varriable
rm -f source /etc/profile.d/jdk_home.sh

JAVA_HOME=$APPS_DIRECTORY/jvm/zulu8/java

cat <<EOT >> /etc/profile.d/jdk_home.sh
#!/bin/sh
export JAVA_HOME=$JAVA_HOME
export PATH=\$JAVA_HOME/bin:$PATH
EOT

source /etc/profile.d/jdk_home.sh

if type -p java; then
    #echo found java executable in PATH
	printf " ${GRN}   => 3.  The java executable is found in PATH  ${NC}\n"
    _java=java
elif [[ -n "$JAVA_HOME" ]] && [[ -x "$JAVA_HOME/bin/java" ]];  then
    #echo found java executable in JAVA_HOME
    printf " ${GRN}   => 3. The java executable is found in JAVA_HOME  ${NC}\n"
	_java="$JAVA_HOME/bin/java"
else
   # echo "no java"
	printf " ${RED}   => 3.  There is no java founded please check your script     ${NC}\n"
	exit
fi

if [[ "$_java" ]]; then
    version=$("$_java" -version 2>&1 | awk -F '"' '/version/ {print $2}')
    # echo version "$version"
    printf " ${GRN}   => 3.  The java installed version is  $version    ${NC}\n"
fi

fi # End install java

if [ "$INSTALL_RE" = true ] ; then
printf " ${GRN} => 4. Start downloading and installing the Remote engine  ..  ${NC}\n"

rm -rf $APPS_DIRECTORY/Talend-RemoteEngine-V2.11.7
cd  $APPS_DIRECTORY/
wget --user-agent Mozilla/4.0 'https://gitlab.com/amar_belgacem/talend-cloud-binaries/-/raw/main/Talend-RemoteEngine-V2.11.7-229.tar.gz' -O re.tar.gz
tar -xf re.tar.gz
rm -f re.tar.gz
sed -i -e 's/us/'$RE_PAIRING_REGIN'/' $APPS_DIRECTORY/Talend-RemoteEngine-V2.11.7/etc/org.talend.ipaas.rt.pairing.client.cfg

cd $APPS_DIRECTORY/Talend-RemoteEngine-V2.11.7/bin


printf " ${YEL} => 5. Starting and installing the Remote engine  ..  ${NC}\n"
source /etc/profile.d/jdk_home.sh
if pgrep -a -f Talend-RemoteEngine > /dev/null
then
printf " ${YEL}  =>  5. there is RemoteEngine java instance that will be killed   ..  ${NC}\n"
kill -9 $(ps aux | grep RemoteEngine | grep -v 'grep' | awk '{print $2}')
sleep 10
fi

rm -f /etc/init.d/Talend-RemoteEngine-V2.11.7-service
./start
sleep 60
./client -u karaf -p karaf feature:install wrapper
./client -u karaf -p karaf "wrapper:install -n Talend-RemoteEngine-V2.11.7"
#./client -u karaf source scripts/configureC2.sh
./stop
sleep 20

ln -s $APPS_DIRECTORY/Talend-RemoteEngine-V2.11.7/bin/Talend-RemoteEngine-V2.11.7-service /etc/init.d/
chkconfig Talend-RemoteEngine-V2.11.7-service --add
chkconfig Talend-RemoteEngine-V2.11.7-service on

sed -i -e 's/#RUN_AS_USER=/RUN_AS_USER='$TLLD_USR_SERVICE'/' /etc/init.d/Talend-RemoteEngine-V2.11.7-service
sed -i -e 's/remote.engine.pre.authorized.key =/remote.engine.pre.authorized.key ='$PAIRING_TOKEN'/' $APPS_DIRECTORY/Talend-RemoteEngine-V2.11.7/etc/preauthorized.key.cfg
sed -i -e 's/max.deployed.flows=3/max.deployed.flows=0/' $APPS_DIRECTORY/Talend-RemoteEngine-V2.11.7/etc/org.talend.ipaas.rt.deployment.agent.cfg
sed -i -e 's/# local.maven.clean.interval.days=0/local.maven.clean.interval.days=30/' $APPS_DIRECTORY/Talend-RemoteEngine-V2.11.7/etc/org.talend.ipaas.rt.deployment.agent.cfg
sed -i -e 's/# local.maven.clean.older.than.days=7/local.maven.clean.older.than.days=30/' $APPS_DIRECTORY/Talend-RemoteEngine-V2.11.7/etc/org.talend.ipaas.rt.deployment.agent.cfg
sed -i -e 's/wrapper.java.maxmemory=512/wrapper.java.maxmemory=1024/' $APPS_DIRECTORY/Talend-RemoteEngine-V2.11.7/etc/Talend-RemoteEngine-V2.11.7-wrapper.conf
echo "http.proxyHost=serverproxy.rpp.aon.net" >> $APPS_DIRECTORY/Talend-RemoteEngine-V2.11.7/etc/system.properties
echo "http.proxyPort=8080" >> $APPS_DIRECTORY/Talend-RemoteEngine-V2.11.7/etc/system.properties
echo "https.proxyHost=serverproxy.rpp.aon.net" >> $APPS_DIRECTORY/Talend-RemoteEngine-V2.11.7/etc/system.properties
echo "https.proxyPort=8080" >> $APPS_DIRECTORY/Talend-RemoteEngine-V2.11.7/etc/system.properties

chown -R $TLLD_USR_SERVICE:$TLLD_USR_SERVICE $APPS_DIRECTORY/Talend-RemoteEngine-V2.11.7

systemctl daemon-reload
systemctl restart Talend-RemoteEngine-V2.11.7-service
sleep 30
systemctl status  Talend-RemoteEngine-V2.11.7-service
ps -ef | grep RemoteEngine

fi #end install remote engine


if [ "$INSTALL_RT" = true ] ; then

printf " ${GRN} => 6. Start downloading and installing the runtime karaf   ..  ${NC}\n"
source /etc/profile.d/jdk_home.sh
rm -rf $APPS_DIRECTORY/Talend-Runtime-V7.3.1
cd  $APPS_DIRECTORY/
wget --user-agent Mozilla/4.0 'https://gitlab.com/amar_belgacem/talend-cloud-binaries/-/raw/main/Talend-Runtime-V7.3.1.tar.gz' -O rt.tar.gz
tar -xf rt.tar.gz
rm -f rt.tar.gz

cd $APPS_DIRECTORY/Talend-Runtime-V7.3.1/bin


printf " ${YEL} => 6. Starting and installing the runtime karaf  ..  ${NC}\n"
if  pgrep -a -f Talend-Runtime > /dev/null
then
printf " ${YEL}  =>  6. there is a Runtime java instance that will be killed   ..  ${NC}\n"
kill -9 $(ps aux | grep Runtime | grep -v 'grep' | awk '{print $2}')
sleep 10
fi

rm -f /etc/init.d/Talend-Runtime-V7.3.1-service
./start
sleep 60
./client -u karaf -p karaf feature:install wrapper
./client -u karaf -p karaf "wrapper:install -n Talend-Runtime-V7.3.1"
#./client -u karaf source scripts/configureC2.sh
./stop
sleep 20

ln -s $APPS_DIRECTORY/Talend-Runtime-V7.3.1/bin/Talend-Runtime-V7.3.1-service /etc/init.d/
chkconfig Talend-Runtime-V7.3.1-service --add
chkconfig Talend-Runtime-V7.3.1-service on

sed -i -e 's/#RUN_AS_USER=/RUN_AS_USER='$TLLD_USR_SERVICE'/' /etc/init.d/Talend-Runtime-V7.3.1-service
sed -i -e 's/url = "https:\/\/localhost.*"/url = * /' $APPS_DIRECTORY/Talend-Runtime-V7.3.1/etc/org.apache.cxf.http.conduits-common.cfg
sed -i '59 i wrapper.java.additional.15=-XX:PermSize=1024m\nwrapper.java.additional.16=-XX:MaxMetaspaceSize=1024m\nwrapper.java.additional.17=-Xms1024m\nwrapper.java.additional.18=-Xmx7168m\nwrapper.java.additional.19=-XX:+UseConcMarkSweepGC\nwrapper.java.additional.20=-XX:+CMSParallelRemarkEnabled' $APPS_DIRECTORY/Talend-Runtime-V7.3.1/etc/Talend-Runtime-V7.3.1-wrapper.conf
sed -i -e 's/wrapper.java.maxmemory=512/#wrapper.java.maxmemory=512/' $APPS_DIRECTORY/Talend-Runtime-V7.3.1/etc/Talend-Runtime-V7.3.1-wrapper.conf
echo "http.proxyHost=serverproxy.rpp.aon.net" >> $APPS_DIRECTORY/Talend-Runtime-V7.3.1/system.properties
echo "http.proxyPort=8080" >> $APPS_DIRECTORY/Talend-Runtime-V7.3.1/system.properties
echo "https.proxyHost=serverproxy.rpp.aon.net" >> $APPS_DIRECTORY/Talend-Runtime-V7.3.1/system.properties
echo "https.proxyPort=8080" >> $APPS_DIRECTORY/Talend-Runtime-V7.3.1/system.properties

chown -R $TLLD_USR_SERVICE:$TLLD_USR_SERVICE $APPS_DIRECTORY/Talend-Runtime-V7.3.1

systemctl daemon-reload
systemctl restart Talend-Runtime-V7.3.1-service
sleep 120
systemctl status  Talend-Runtime-V7.3.1-service
ps -ef | grep Runtime

####################################################### patching the runtime ###################
printf " ${YEL} => 6. ============================== Start Patching the runtime ==================================  ..  ${NC}\n"

cd  $APPS_DIRECTORY/
rm -rf patch
rm -f patch.zip
wget --user-agent Mozilla/4.0 'https://gitlab.com/amar_belgacem/talend-cloud-binaries/-/raw/main/Patch_20220107_R2022-01_v1-RT-7.3.1.zip' -O patch.zip
unzip patch.zip -d patch
rm -f patch.zip
yes | cp -rf $APPS_DIRECTORY/patch/container/*  $APPS_DIRECTORY/Talend-Runtime-V7.3.1
chown -R $TLLD_USR_SERVICE:$TLLD_USR_SERVICE $APPS_DIRECTORY/Talend-Runtime-V7.3.1
sed -i -e 's/sleep 30/sleep 120 /' $APPS_DIRECTORY/Talend-Runtime-V7.3.1/patches/Patch_20220107_R2022-01_v1-RT-7.3.1/patch.sh
$APPS_DIRECTORY/Talend-Runtime-V7.3.1/patches/Patch_20220107_R2022-01_v1-RT-7.3.1/patch.sh
rm -rf patch
systemctl restart Talend-Runtime-V7.3.1-service

############################################################################### Firewalls rules incase ###################################
firewall-cmd --add-port=8040/tcp --permanent
firewall-cmd --add-port=9001/tcp --permanent
firewall-cmd --add-service=http  --permanent
firewall-cmd --add-service=https --permanent
firewall-cmd --reload

fi # End install runtime
